package com.chrisnor.tp01_tombeau_chrisnor;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import static java.lang.Integer.parseInt;

public class MainActivity extends AppCompatActivity {
    private Button buttonToast;
    private Button buttonInc;
    private TextView textViewCount;

    int i = 1;
    int j=0;
    private void initViews() {
        buttonToast = findViewById(R.id.toast);
        buttonInc = findViewById(R.id.increment);
        textViewCount = findViewById(R.id.textCounter);
        //int counter= counter + buttonInc;

        // Intercept click on the compute button

            buttonInc.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    //incrémenter la valeur du compteur puis mettre à jour le text
                    i++;
                    textViewCount.setText("" + i);
                    j++;
                }

            });

        buttonToast.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                //incrémenter la valeur du compteur puis mettre à jour le text
// Pour afficher un toast
                Toast.makeText(MainActivity.this, " Vous avez cliqué: " + j + " fois", Toast.LENGTH_SHORT).show();
            }
        });

    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();

    }
}
